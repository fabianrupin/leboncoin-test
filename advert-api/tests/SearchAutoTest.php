<?php

namespace App\Tests;

use App\Service\SearchAutoService;
use PHPUnit\Framework\TestCase;

class SearchAutoTest extends TestCase
{
    public function testAudiCar(): void
    {
        $vehicleInput = 'rs4 avant';

        $searchAutoService = new SearchAutoService();

        $result = $searchAutoService->findAuto($vehicleInput);

        $this->assertEquals('Audi', $result['manufacturer']);
        $this->assertEquals('Rs4', $result['model']);
    }

    public function testCitroenCar(): void
    {
        $vehicleInput = 'ds 3 crossback';

        $searchAutoService = new SearchAutoService();

        $result = $searchAutoService->findAuto($vehicleInput);

        $this->assertEquals('Citroën', $result['manufacturer']);
        $this->assertEquals('Ds3', $result['model']);
    }

    public function testCitroenCar2(): void
    {
        $vehicleInput = 'CrossBack ds 3';

        $searchAutoService = new SearchAutoService();

        $result = $searchAutoService->findAuto($vehicleInput);

        $this->assertEquals('Citroën', $result['manufacturer']);
        $this->assertEquals('Ds3', $result['model']);
    }

    public function testBMWCar(): void
    {
        $vehicleInput = 'Gran Turismo Série5';

        $searchAutoService = new SearchAutoService();

        $result = $searchAutoService->findAuto($vehicleInput);

        $this->assertEquals('BMW', $result['manufacturer']);
        $this->assertEquals('Serie 5', $result['model']);
    }
}
