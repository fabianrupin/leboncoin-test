<?php

namespace App\Controller;

use App\Entity\Advert;
use App\Model\AdvertDTO;
use App\Projection\ErrorProjection;
use App\Repository\CategoryRepository;
use App\Service\SearchAutoService;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;

class AdvertController extends AbstractFOSRestController
{
    /**
     * @Rest\Get(
     *     path = "/advert/{id}",
     *     name = "advert_show",
     *     requirements={"id"="\d+"},
     * )
     */
    public function show(Advert $advert): Response
    {
        $view = $this->view($advert, 200);

        return $this->handleView($view);
    }

    /**
     * @Rest\Post(
     *     path = "/advert",
     *     name = "advert_create"
     * )
     * @ParamConverter("advertDTO", converter="fos_rest.request_body")
     */
    public function create(AdvertDTO $advertDTO, EntityManagerInterface $entityManager, CategoryRepository $repository, SearchAutoService $searchAuto): Response
    {
        $category = $repository->findOneBy(['id' => $advertDTO->getCategory()]);

        if (!$category) {
            $view = $this->view(null, 404);

            return $this->handleView($view);
        }

        $advert = new Advert();
        $advert->setTitle($advertDTO->getTitle());
        $advert->setContent($advertDTO->getContent());
        $advert->setCategory($category);

        if ('Automobile' === $category->getName()) {
            $vehicle = $searchAuto->findAuto($advertDTO->getVehicle());

            if (!$vehicle) {
                $error = new ErrorProjection(
                    'Vehicle not found',
                    'You try to make an advert in automobile category. you must enter a known model from our database',
                    404
                );
                $view = $this->view($error, 404);

                return $this->handleView($view);
            }
            $advert->setVehicleMake($vehicle['manufacturer']);
            $advert->setVehicleModel($vehicle['model']);
        }

        $entityManager->persist($advert);

        $entityManager->flush();

        $view = $this->view($advert, 201);

        return $this->handleView($view);
    }

    /**
     * @Rest\Put(
     *     path = "/advert/{id}",
     *     name = "advert_edit",
     *     requirements = {"id"="\d+"}
     * )
     * @ParamConverter("newAdvert", converter="fos_rest.request_body")
     */
    public function edit(Advert $advert, Advert $newAdvert, EntityManagerInterface $entityManager): Response
    {
        $advert->setTitle($newAdvert->getTitle());
        $advert->setContent($advert->getContent());

        $entityManager->flush();

        $view = $this->view($advert, 200);

        return $this->handleView($view);
    }

    /**
     * @Rest\View(StatusCode = 204)
     * @Rest\Delete(
     *     path = "/advert/{id}",
     *     name = "advert_delete",
     *     requirements = {"id"="\d+"}
     * )
     */
    public function delete(Advert $advert, EntityManagerInterface $entityManager): Response
    {
        $entityManager->remove($advert);

        $entityManager->flush();

        $view = $this->view(null, 204);

        return $this->handleView($view);
    }
}
