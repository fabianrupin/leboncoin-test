<?php

namespace App\DataFixtures;

use App\Factory\AdvertFactory;
use App\Factory\CategoryFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        CategoryFactory::createOne([
            'name' => 'Emploi',
        ]);
        CategoryFactory::createOne([
            'name' => 'Automobile',
        ]);
        CategoryFactory::createOne([
            'name' => 'Immobilier',
        ]);

        AdvertFactory::createMany(20);

        $manager->flush();
    }
}
