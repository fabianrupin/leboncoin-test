<?php

namespace App\Projection;

class ErrorProjection
{
    public function __construct(private string $errorName, private string $message, private int $statusCode)
    {
    }

    public function getErrorName(): string
    {
        return $this->errorName;
    }

    public function setErrorName(string $errorName): void
    {
        $this->errorName = $errorName;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function setMessage(string $message): void
    {
        $this->message = $message;
    }

    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    public function setStatusCode(int $statusCode): void
    {
        $this->statusCode = $statusCode;
    }
}
