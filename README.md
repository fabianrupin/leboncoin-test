# LeBonCoin Technical Test

A technical test for a Symfony developer offer at LeBonCoin

## Description

## Documentation

### Install

#### Clone
To begin using this project, clone the repo and install back dependencies:

```bash
git clone git@gitlab.com:fabianrupin/leboncoin-test.git
cd leboncoin-test
```

#### Build 

If not already done, install Docker Compose
1. Run `docker-compose build --pull --no-cache` to build fresh images
2. Run `docker-compose up -d`
3. Access the container shell : `docker exec -it www_leboncoin_test bash`
4. Install dependencies : `composer install`

#### Install database

From the shell of the container:
1. Run `symfony console doctrine:database:create` to create database
2. Run `symfony console doctrine:migration:migrate` to install the database structure
3. Run `symfony console doctrine:fixtures:load` to install fixtures in development

Run `docker-compose down --remove-orphans` to stop the Docker containers.

### Use

The API is available at localhost:8741, and adverts endpoint is available at http://localhost:8741/advert
1. Show an advert : advert/{id} with GET Header
2. Create an advert : advert with POST Header, and this parameters :
   1. title (string)
   2. content (string)
   3. category (int)
   4. vehicle (string)
3. Edit an advert : advert/{id} with PUT Header
4. Delete an advert : advert/{id} with DELETE Header

### Examples
   Get an advert : `curl http://localhost:8741/advert/1`

   Create an advert ```curl -X POST -H "Content-Type: application/json"
      -d '{"title": "nouvelle annonce", "content": "mon contenu", "category":2, "vehicle": "ds3"}'
      http://localhost:8741/advert```

### Test
To run tests, run the following command:
```bash
./bin/phpunit
```

## Authors
- [@fabianrupin](https://www.gitlab.com/fabianrupin)